import XMonad
import XMonad.Config.Desktop

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import System.IO
import XMonad.Actions.SpawnOn
import XMonad.Util.SpawnOnce

import XMonad.Layout
import XMonad.Layout.LayoutCombinators hiding ((|||))
import XMonad.Layout.BinarySpacePartition
import XMonad.Layout.LayoutHints
import XMonad.Layout.ResizableTile
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spacing

altMask :: KeyMask
altMask = mod1Mask
ctrlMask :: KeyMask
ctrlMask = controlMask
modm :: KeyMask
modm = mod4Mask

main :: IO ()
main = do xmproc <- spawnPipe "xmobar -d"
          xmonad $ desktopConfig
                  { manageHook = manageDocks <+> manageHook defaultConfig
                  , layoutHook = spacingRaw True (Border 0 0 0 0) False (Border 10 10 10 10) True $ avoidStruts  $ emptyBSP ||| ResizableTall 0 (3/100) (1/2) [] ||| Full ||| simplestFloat
                  , logHook = dynamicLogWithPP xmobarPP
                          { ppOutput = hPutStrLn xmproc
                          -- , ppTitle = (\x -> [])
                          , ppTitle = xmobarColor "green" "" . shorten 40
                          , ppSep = " :: "
                          , ppWsSep = " -> "
                          , ppOrder = reverse
                          }
                  , terminal = "st"
                  , modMask = mod4Mask
                  , borderWidth = 1
                  , focusedBorderColor = "#000000"
                  , workspaces = ["yksi", "kaksi", "kolme", "neljä", "viisi", "kuusi", "seitsemän", "kahdeksan", "yhdeksän"]
                  , startupHook = do spawn "(pkill stalone; sleep 2 && stalonetray)"
                                     spawn "nm-applet"
                  }
                  `additionalKeys` [ ((mod4Mask, xK_p), spawn "rofi -show run")
                                   -- , ((mod4Mask,     xK_a), sendMessage MirrorShrink)
                                   -- , ((mod4Mask,     xK_z), sendMessage MirrorExpand)
                                   , ((modm,               xK_a),     sendMessage Balance)
                                   , ((modm .|. shiftMask, xK_a),     sendMessage Equalize)
                                   , ((modm .|. altMask,               xK_l     ), sendMessage $ ExpandTowards R)
                                   , ((modm .|. altMask,               xK_h     ), sendMessage $ ExpandTowards L)
                                   , ((modm .|. altMask,               xK_j     ), sendMessage $ ExpandTowards D)
                                   , ((modm .|. altMask,               xK_k     ), sendMessage $ ExpandTowards U)
                                   , ((modm .|. altMask .|. ctrlMask , xK_l     ), sendMessage $ ShrinkFrom R)
                                   , ((modm .|. altMask .|. ctrlMask , xK_h     ), sendMessage $ ShrinkFrom L)
                                   , ((modm .|. altMask .|. ctrlMask , xK_j     ), sendMessage $ ShrinkFrom D)
                                   , ((modm .|. altMask .|. ctrlMask , xK_k     ), sendMessage $ ShrinkFrom U)
                                   , ((modm,                           xK_r     ), sendMessage Rotate)
                                   , ((modm,                           xK_s     ), sendMessage Swap)
                                   , ((modm,                           xK_n     ), sendMessage FocusParent)
                                   , ((modm .|. ctrlMask,              xK_n     ), sendMessage SelectNode)
                                   , ((modm .|. shiftMask,             xK_n     ), sendMessage MoveNode)
                                   , ((modm .|. shiftMask,             xK_equal     ), incWindowSpacing 1)
                                   , ((modm,                           xK_minus     ), decWindowSpacing 1)
                                   , ((modm,                           xK_equal     ), toggleWindowSpacingEnabled)
                                   , ((altMask .|. ctrlMask,           xK_l     ), spawn "lck")
                                   ]
