(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (yasnippet-snippets yasnippet yaml-mode vue-mode evil-org markdown-mode shakespeare-mode go-mode flycheck vimrc-mode haskell-mode rainbow-delimiters rainbow-mode flucui-themes auto-complete desktop+ evil-surround dumb-jump evil)))
 '(safe-local-variable-values
   (quote
    ((haskell-indent-spaces . 4)
     (haskell-process-use-ghci . t)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(evil-mode 1)
(setq evil-want-fine-undo t) ;; vim-like undo
(global-evil-surround-mode 1)
(global-set-key (kbd "C-h") 'help) ;; evil fucks this binding

(dumb-jump-mode)

(ac-config-default)
(global-auto-complete-mode t)

;; general config
(global-set-key (kbd "C-x C-b") 'buffer-menu)
(electric-pair-mode)            ; auto-pair
(setq-default electric-pair-inhibit-predicate 'electric-pair-conservative-inhibit) ; auto-pair only when needed
(setq backup-directory-alist `(("." . "~/.emacs.d/saves")))
(setq-default indent-tabs-mode t)
(savehist-mode 1)               ; history accross sessions
(filesets-init)
(setq scroll-conservatively 2000) ; scroll by line, not half a screen
(global-auto-revert-mode 1)     ; automaticaly refresh files
(add-hook 'after-init-hook #'global-flycheck-mode)
(yas-global-mode 1)
(define-key yas-minor-mode-map (kbd "C-c y") 'yas-insert-snippet)

;; language specific config
(add-hook 'sgml-mode-hook 'emmet-mode) ;; Auto-start on any markup modes
(setq-default c-basic-offset 8
	      c-default-style "linux"
	      tab-width 8
	      indent-tabs-mode t)

;; ui config
(add-hook 'after-init-hook (lambda () (scroll-bar-mode -1)))
(add-hook 'after-init-hook (lambda () (menu-bar-mode -1)))
(add-hook 'after-init-hook (lambda () (tool-bar-mode -1)))
(setq column-number-mode t)
(icomplete-mode)                ; better minibuffer completion

;; theme + hightlighting
(flucui-themes-load-style 'light)					; flucui-light theme
(add-hook 'find-file-hook (lambda () (rainbow-mode t)))
(add-hook 'find-file-hook (lambda () (rainbow-delimiters-mode t)))
(set-frame-font "Terminus-12")
(require 'whitespace)
(global-whitespace-mode t)
(setq whitespace-line-column 79
      whitespace-style '(face tabs trailing lines-tail space-before-tab indentation empty space-after-tab tab-mark))


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(fixed-pitch ((t nil))))
